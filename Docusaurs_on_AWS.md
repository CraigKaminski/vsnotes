---
tags:
  - AWS
---

# Docusaurs on AWS

## Link
- [Bitbucket](https://bitbucket.org)
- [Amazon API Gateway](https://aws.amazon.com/api-gateway/)
- [AWS Lambda](https://aws.amazon.com/lambda/)
- [Amazon S3](https://aws.amazon.com/s3/)
- [Git Webhooks with AWS Services](https://aws.amazon.com/quickstart/architecture/git-to-s3-using-webhooks/)
- [Connect Your Git Repository to Amazon S3](https://aws.amazon.com/about-aws/whats-new/2017/09/connect-your-git-repository-to-amazon-s3-and-aws-services-using-webhooks-and-new-quick-start/)
- [AWS Lambda Container Image Support](https://aws.amazon.com/blogs/aws/new-for-aws-lambda-container-image-support/)
- [A Shared File System for Your Lambda Functions](https://aws.amazon.com/blogs/aws/new-a-shared-file-system-for-your-lambda-functions/)

## Components
- Bitbucket
- Amazon API Gateway
- AWS Lambda
- Amazon S3